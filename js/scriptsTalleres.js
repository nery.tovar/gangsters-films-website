$(document).ready(function() {
    $('#sliderTaller').slippry();
    AOS.init();
  });
  
  window.onscroll = function() {
      scrollFunction();
  };
  
  function scrollFunction() {
    if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 1) {
      $(".navbar").css("background-color", "rgba(0, 0, 0, 0.4)");
      $(".dropdown-menu").css("background-color", "rgba(0, 0, 0, 0.4)");
      $("#logo").attr("src", "../img/img-logos/Gangsters-Films-logo.png");
      $(".nav-link").addClass("text-white");
      $(".dropdown-item").addClass("text-white");
      $(".dropdown-divider").addClass("text-white");
    } else {
      $(".navbar").css("background-color", "#000000");
      $(".navbar").css("color", "#ffffff");
      $(".dropdown-menu").css("background-color", "#000000");
      $("#logo").attr("src", "../img/img-logos/Gangsters-Films-logo.png");
      $(".nav-link").removeClass("text-dark");
      $(".dropdown-item").removeClass("text-dark");
      $(".dropdown-divider").removeClass("text-dark");
    }
  }
  
  